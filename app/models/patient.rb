class Patient < ActiveRecord::Base
  belongs_to :insurance

  has_many :appointments
  has_many :specialists, :through => :appointments


end
