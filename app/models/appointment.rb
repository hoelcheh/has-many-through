class Appointment < ActiveRecord::Base

  belongs_to :patient
  belongs_to :specilist

  def days_until_appointment
    (appointment_date - Date.today).to_int
  end
end
